-- chikun :: 2015
-- Main skeleton for KAE

require 'lib/bindings'
require 'lib/class'
require 'lib/drawing'
require 'lib/maths'
require 'src/loadGFX'
require 'src/bh/bh'
require 'src/bh/loadBullets'
require 'src/bh/loadShips'
require 'src/vn/vn'
require 'src/vn/controller'
require 'src/vn/loadChars'
require 'src/vn/loadScenes'

function love.load()

  -- Set current mode to visual novel
  currentMode = vn

  -- Load into current mode
  currentMode.load()

  -- Canvas for drawing
  gameCanvas = lg.newCanvas(800, 600)

end


function love.update(dt)

  -- Limit game speed if FPS drops below 15 to save calculations
  if dt > 1/15 then dt = 1/15 end

  -- Cursor object for use in functions
  cursor = {
    x = (love.mouse.getX() / lw.getWidth() ) * 800,
    y = (love.mouse.getY() / lw.getHeight()) * 600,
    radius = 0
  }

  -- Update current mode
  currentMode.update(dt)

end


function love.draw()

  -- Clear canvas
  gameCanvas:clear()

  -- Set target
  lg.setCanvas(gameCanvas)

  -- Draw current mode
  currentMode.draw()

  -- Unset target
  lg.setCanvas()

  -- Draw canvas
  lg.setColor(255, 255, 255)
  lg.draw(gameCanvas, 0, 0, 0, lw.getWidth() / 800, lw.getHeight() / 600)

end


function love.mousepressed(x, y, mb)

  if (mb == "l" and currentMode.click) then

    currentMode:click()

  end

end

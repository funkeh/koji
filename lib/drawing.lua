-- chikun :: 2015
-- Extra drawing functions


function lg.printWithShadow(text, x, y)

  -- Save old colour
  local previousColour = {lg.getColor()}

  -- Draw shadow
  lg.setColor(0, 0, 0, previousColour[4] * 0.75)
  lg.print(text, x + 1, y + 1)

  -- Reset colour
  lg.setColor(previousColour[1], previousColour[2],
    previousColour[3], previousColour[4])

  -- Draw main text
  lg.print(text, x, y)

end


function lg.drawCentred(image, x, y)

  -- Draws image centred on point
  lg.draw(image, x, y, 0, 1, 1, image:getWidth() / 2, image:getHeight() / 2)

end

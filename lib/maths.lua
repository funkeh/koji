-- chikun :: 2015
-- Extra maths functions

function math.distance(x1, y1, x2, y2)

  return ((x2 - x1) ^ 2 + (y2 - y1) ^ 2) ^ 0.5

end


function math.doesIntersect(obj1, obj2)

  local value = ((obj2.x - obj1.x) ^ 2 + (obj2.y - obj1.y) ^ 2) ^ 0.5

  return (value < (obj1.radius + obj2.radius))

end

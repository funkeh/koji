-- chikun :: 2015
-- Configuration file

function love.conf(game)

  -- Set resolution
  game.window.width = 800
  game.window.height = 600

  -- Can resize game window
  game.window.resizable = true

  -- Set title
  game.window.title = "Koji's Anxiety Extreme"
  game.window.vsync = false

end

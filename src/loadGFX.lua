-- chikun :: 2015
-- Graphics management

-- Graphics table
gfx = { }
graphics = { }


-- Unload all graphics
function graphics:unload(gfxTable)

  -- Iterates through table to nullify all images
  local function unloadTable(table)
    for k, v in ipairs(table) do

      -- Nullification
      if type(v) == "userdata" then
        table[i] = nil

      -- Iteration
      elseif type(v) == "table" then
        unloadTable(v)
        table[i] = nil
      end

    end
  end

  -- Unload all graphics
  unloadTable(gfxTable or gfx)

  -- Garbage collection
  collectgarbage()

end

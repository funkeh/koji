-- chikun :: 2015
-- Controller

controllerVN = class(function (newController, scene, screen)
    -- The useful count variable
    newController.count = 0

    -- Set placeholder scene
    newController.mode = "fade"
    newController.bg = "clear"
    newController.scene = nil
    newController.screen = nil
    newController.speaker = ""
    actors = { }

    -- Set scene to be faded into
    newController.nextBg = scene[screen].bg or "clear"
    newController.nextScene = scene
    newController.nextScreen = screen

    newController.menuButton = {
      x = 748,
      y = 48,
      image = gfx.menuButton,
      radius = 38
    }
  end)


function controllerVN:update(dt)

  -- Increase counter, capping at 1
  self.count = math.min(1, self.count + dt)

  -- If in fade state and fully counted
  if self.mode == "fade" and self.count == 1 then

    self:moveToText()

  end

end


function controllerVN:click()

  if self.mode == "fade" then

    self:moveToText()

  elseif self.mode == "text" and self.count < 1 then

    self.count = 1

  else

    self:moveToNext()

  end

end


function controllerVN:moveToText()

  -- Update sceen / screen
  self.mode = "text"
  self.count = 0
  self.bg = self.nextBg or self.bg
  self.scene = self.nextScene
  self.screen = self.nextScreen
  self.speaker = self.scene[self.screen].speaker or ""
  if self.scene[self.screen].code then
    self.scene[self.screen].code()
  end

  -- Remove next scene variables
  self.nextBg = nil
  self.nextScene = nil
  self.nextScreen = nil

end


function controllerVN:moveToNext()

  local screen = self.scene[self.screen]
  local nextScene, nextScreen, nextBg =
    screen.nextScene or self.scene,
    screen.nextScreen or self.screen + 1,
    self.bg

  if nextScene ~= self.scene then

    nextScene = scenes[nextScene]

    nextScreen = screen.nextScreen or 1

    nextBg = nextScene.bg or self.bg

  else

    nextBg = self.scene[nextScreen].bg or nextBg

  end

  self.count = 0
  local nextSpeaker = nextScene[nextScreen].speaker or self.speaker

  if nextScene ~= self.scene or nextBg ~= self.bg or nextSpeaker ~= self.speaker then

    self.mode = "fade"
    self.nextBg = nextBg
    self.nextScene = nextScene
    self.nextScreen = nextScreen
    self.nextSpeaker = nextSpeaker

  else

    self.mode = "text"
    self.bg = nextBg
    self.scene = nextScene
    self.screen = nextScreen
    if self.scene[self.screen].code then
      self.scene[self.screen].code()
    end

  end

end


function controllerVN:draw()

  -- Determine current screen
  local screen = { }
  if controller.scene then
    screen = controller.scene[controller.screen]
  end

  -- Draw current background image
  lg.setColor(255, 255, 255)
  lg.draw(gfx.bg[controller.bg], 0, 0)

  for key, actor in ipairs(actors) do

    local img = actor.char.gfx[actor.char.stance]
    local x = 800 * (key / (#actors + 1))
    local xScale = 1
    if x > 400 then xScale = -1 end
    xScale = xScale * 0.9

    lg.draw(img, x, 600, 0, xScale, 0.9, img:getWidth() / 2, img:getHeight())

  end

  -- If controller is in its fade state
  if controller.mode == "fade" then


    -- Determine next screen
    local nextScreen = controller.nextScene[controller.nextScreen]


    -- If backgrounds differ
    if controller.bg ~= controller.nextBg then

      -- Fade in new background
      lg.setColor(255, 255, 255, 255 * controller.count)
      lg.draw(gfx.bg[controller.nextBg], 0, 0)

    end


    -- Determine next speaker
    local nextSpeaker = nextScreen.speaker or self.speaker

    -- If next speaker is different to current speaker
    if nextSpeaker ~= controller.speaker then

      -- Fade out current speaker
      lg.setColor(255, 255, 255, 255 * (1 - controller.count))
      lg.printWithShadow(controller.speaker, speakerPos.x, speakerPos.y)

      -- Fade in new speaker
      lg.setColor(255, 255, 255, 255 * controller.count)
      lg.printWithShadow(nextSpeaker, speakerPos.x, speakerPos.y)

    -- Otherwise...
    else

      -- ...draw current speaker
      lg.printWithShadow(controller.speaker, speakerPos.x, speakerPos.y)

    end


    -- Determine chat box alpha
    local alpha = 0
    if screen.text and nextScreen.text then
      alpha = 1
    elseif not screen.text and nextScreen.text then
      alpha = controller.count
    elseif screen.text and not nextScreen.text then
      alpha = (1 - controller.count)
    end

    -- Set chat box alpha
    lg.setColor(255, 255, 255, 255 * alpha)

    -- Draw chat box
    lg.draw(gfx.chat, 50, 450)

  else

    if screen.text then

      -- Draw cute anime gril
      --lg.draw(gfx.cecile, 400 - gfx.cecile:getWidth() / 2, 600 - gfx.cecile:getHeight())

      -- Draw chat box
      lg.draw(gfx.chat, 50, 450)

      -- Speaker name
      lg.printWithShadow(controller.speaker, speakerPos.x, speakerPos.y)

      -- Draw text body, accounting for roll
      lg.printf(screen.text:sub(1, math.floor(screen.text:len() * controller.count)),
        60, 460, 730, 'left')

    end

  end


  -- Reset colour
  lg.setColor(255, 255, 255)

  -- Draw menu button
  lg.drawCentred(self.menuButton.image, self.menuButton.x, self.menuButton.y)

end



function controllerVN:addActor(actorName)

  -- Add actor to scene
  table.insert(actors, {
      tag = actorName,
      char = chars[actorName]
    })

end



function controllerVN:removeActor(actorName)

  -- Add actor to scene
  for key, value in ipairs(actors) do

    if value.tag == actorName then

      table.remove(actors, key)

    end

  end

end

-- chikun :: 2015
-- Hisako character

charHisako = class(character, function(hisako, stance)
    character.init(hisako, "Hisako Miyamoto", stance)

    hisako.gfx = {
      normal = lg.newImage("/gfx/char/cecile/shock.png")
    }
  end)

return charHisako()

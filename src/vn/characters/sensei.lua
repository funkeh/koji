-- chikun :: 2015
-- Sensei character

charSensei = class(character, function(sensei, stance)
    character.init(sensei, "Sensei", stance)

    sensei.gfx = {
      normal = lg.newImage("/gfx/char/sensei/normal.png")
    }
  end)

return charSensei()

-- chikun :: 2015
-- Scene 1 for prototype


return {
  {
    bg = "cafeteria",
    speaker = "",
    text =
      "The cafeteria isn't anywhere close to a fine dining\n" ..
      "experience. The food is cheap and disgusting. So much so\n" ..
      "that most students bring their own lunch."
  },
  {
    text =
      "But for someone without money, like me... it's perfect."
  },
  {
    text =
      "I grab my lunch and find a good spot to sit."
  },
  {
    text =
      "It's only been two weeks since I transferred to this school.\n" ..
      "The doctors recommended for me to get away from the\n" ..
      "city and live in the country."
  },
  {
    text =
      "They said it would help me clear my head, a bit of fresh air\n" ..
      "to help soothe the soul."
  },
  {
    text =
      "However, it's not that much different from the city. People\n" ..
      "still treat me the same here. They don't talk to me, look at\n" ..
      "me or even acknowledge me."
  },
  {
    text =
      "It's not like I'm a ghost or anything, they did try to talk to\n" ..
      "me. I just pushed everyone away. I don't need any friends."
  },
  {
    text =
      "It's simpler that way."
  },
  {
    text =
      "*bell rings*"
  },
  {
    text =
      "Noticing everyone hurry off to class, I look at my unfinished\n" ..
      "lunch."
  },
  {
    nextScene = "misako2",
    text =
      "It would be a waste of a meal if I don't finish it. Might as\n" ..
      "well take my time."
  }
}

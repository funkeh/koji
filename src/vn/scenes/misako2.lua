-- chikun :: 2015
-- Scene 2 for prototype


return {
  {
    bg = "hallway",
    speaker = "",
    text =
      "After missing the better part of my PE lesson I quickly get\n" ..
      "changed and begin to head to the oval. Unfortunately I'll\n" ..
      "have to pass the staff room."
  },
  {
    text =
      "I've been there before for being late, and I'd rather not\n" ..
      "be lectured by Sensei again."
  },
  {
    text =
      "Arriving at the staff room I soften my footsteps and move\n" ..
      "quickly. Hopefully they won't notice me."
  },
  {
    text =
      "However I notice two familiar voices coming from the staff\n" ..
      "room. I stop and poke my head into the room."
  },
  {
    text =
      "Sitting at a desk my homeroom teacher and the class\n" ..
      "president Hisako are talking to each other. Not really being\n" ..
      "interested in their conversation I move away from the staff\n" ..
      "room."
  },
  {
    speaker = "Sensei",
    text =
      "Oh, before you go I wanted to talk to you about Koji..."
  },
  {
    speaker = "",
    text =
      "Huh?"
  },
  {
    text =
      "Koji is my name, so their most likely talking about me.\n" ..
      "Concerned I move back to the door and listen in."
  },
  {
    speaker = "Hisako",
    text =
      "What about Koji? Is he in trouble again?"
  },
  {
    speaker = "Sensei",
    text =
      "No, but have you had a chance to talk to him yet?"
  },
  {
    speaker = "Hisako",
    text =
      "No. He doesn't seem to be opening up to anyone, myself\n" ..
      "included. Why is he like this?"
  },
  {
    speaker = "Sensei",
    text =
      "I already told you, it's not something I can talk about."
  },
  {
    speaker = "Hisako",
    text =
      "..."
  },
  {
    speaker = "Sensei",
    text =
      "I know, you're worried about him. For the mean time can\n" ..
      "you keep an eye on him?"
  },
  {
    speaker = "Hisako",
    text =
      "Yes, Sensei..."
  },
  {
    speaker = "Sensei",
    text =
      "Thank you. Now, before you go..."
  },
  {
    speaker = "",
    text =
      "What? Sensei has Hisako watching me? As long as she\n" ..
      "doesn'ttalk to me or anything it shouldn't be an issue."
  },
  {
    nextScene = "misako3",
    text =
      "I make my way to my PE class."
  }
}

-- chikun :: 2015
-- Scene 3 for prototype


return {
  {
    bg = "classroom",
    speaker = "",
    text =
      "*End of the day bell*"
  },
  {
    text =
      "Finally..."
  },
  {
    speaker = "Sensei",
    text =
      "Before you all head out, Hisako has an announcement.",
    code = function()
      controllerVN:addActor("sensei")
    end
  },
  {
    speaker = "",
    text =
      "The class lets out a collective groan."
  },
  {
    speaker = "Sensei",
    text =
      "It won't take long. Hisako?"
  },
  {
    speaker = "",
    text =
      "Huh",
    code = function()
      controllerVN:addActor("hisako")
      controllerVN:removeActor("sensei")
    end
  },
  {
    text =
      "I've never noticed before, but Hisako is kinda cute...",
  },
  {
    speaker = "Hisako",
    text =
      "Currently, the student council is looking for more members.\n" ..
      "If you are interested, please meet me at the council room.\n" ..
      "Thank you."
  },
  {
    speaker = "Sensei",
    text =
      "Thank you Hisako. Class dismissed.",
    code = function()
      controllerVN:addActor("sensei")
      controllerVN:removeActor("hisako")
    end
  }
}

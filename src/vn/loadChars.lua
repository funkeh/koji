-- chikun :: 2015
-- Loads characters


-- Character base class
character = class(function (newChar, name, stance)
    newChar.name = name                   -- Name of character
    newChar.stance = stance or "normal"   -- Stance of character
  end)


function character:setStance(stance)

  self.stance = stance

end


function character:kill()

  graphics:unload(self.gfx)

  self = nil

end

-- Load characters
chars = {
  hisako = require "src/vn/characters/hisako",
  sensei = require "src/vn/characters/sensei"
}

vn = { }



function vn.loadIMG()

  gfx = {
    bg = {
      clear = lg.newImage("gfx/bg/clear.png"),
      cafeteria = lg.newImage("gfx/bg/cafeteria.jpg"),
      classroom = lg.newImage("gfx/bg/classroom.jpg"),
      countryRoad = lg.newImage("gfx/bg/countryRoad.jpg"),
      hallway = lg.newImage("gfx/bg/hallway.jpg")
    },
    chat = lg.newImage("gfx/ui/chat.png"),
    menuButton = lg.newImage("gfx/ui/menu.png")
  }

end


function vn.load()

  vn.loadIMG()

  lg.setNewFont(24)

  controller = controllerVN(scenes.misako1, 1)

  speakerPos = {
    x = 10,
    y = 413
  }

end


function vn.update(dt)

  -- Update counter
  controller:update(dt)

end


function vn.draw()

  controller:draw()

end


function vn.click()

  controller:click()

end

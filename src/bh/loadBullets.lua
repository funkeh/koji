-- chikun :: 2015
-- Loads bullets and works with classes


-- Bullet base class
bullet = class(function (newBullet, x, y, radius, image, xSpeed, ySpeed)
    newBullet.image = image     -- Image this bullet will use
    newBullet.radius = radius   -- Radius of ship's collision circle
    newBullet.x = x
    newBullet.y = y

    newBullet.xSpeed = xSpeed   -- Horizontal speed of bullet
    newBullet.ySpeed = ySpeed   -- Vertical speed of bullet
  end)


function bullet:update(dt)

  -- Move bullet
  self.x = self.x + (self.xSpeed * dt)
  self.y = self.y + (self.ySpeed * dt)

end


function bullet:draw()

  -- Draw bullet
  lg.draw(self.image, self.x, self.y, 0, 1, 1,
    self.image:getWidth() / 2, self.image:getHeight() / 2)

end


ammoPrimary = class(bullet, function(newPrimary, x, y)
    bullet.init(newPrimary, x, y, 3, gfx.ammoPrimary, 0, -1024)
  end)

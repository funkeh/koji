-- chikun :: 2015
-- Loads ships and works with classes

-- Ship base class
ship = class(function (newShip, x, y, radius, image, health)
    newShip.health = health   -- How many hits the ship takes before death
    newShip.image = image     -- The table of images this ships will cycle through
    newShip.radius = radius   -- Radius of ship's collision circle
    newShip.x = x
    newShip.y = y
    newShip.yStart = y

    newShip.invuln = 0  -- Timer on ship's invulnerability. Vulnerabe if 0.
    newShip.step = 0    -- Current step of ship's animation loop.
  end)


function ship:update(dt)

  -- Update animation loop
  self.step = (self.step + dt * 5) % 2

  -- Update vulnerability
  self.invuln = math.max(0, self.invuln - dt)

  self.y = self.yStart + getEnemyYOffset()

end


function ship:isVulnerable()

  -- Returns if ship is vulnerable
  return (self.invuln == 0)

end


function ship:setVulnerable(vul)

  -- Sets ship to vulnerable or not
  if vul then
    self.invuln = 0
  else
    self.invuln = 1
  end

end


function ship:draw()

  -- Set drawing colour, taking invulnerability into consideration
  lg.setColor(255, 255, 255, 255 - math.ceil(self.invuln) * 127)

  -- Determine image to draw
  local image = self.image[math.floor(self.step + 1)]

  -- Actually draw image
  lg.draw(image, self.x, self.y, 0, 1, 1,
    image:getWidth() / 2, image:getHeight() / 2)

end


-- Load the actual ships
require 'src/bh/ships/proto'

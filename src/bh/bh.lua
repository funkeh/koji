bh = {}

function bh.loadIMG()

  gfx = {
    bgImage = lg.newImage("gfx/bh/bg.png"),
    ammoPrimary = lg.newImage("gfx/bh/bullet.png"),
    koji = lg.newImage("gfx/bh/koji.png"),
    shipProto = {
      lg.newImage("gfx/bh/enemy1.png"),
      lg.newImage("gfx/bh/enemy2.png")
    }
  }

end

function bh.load()

  bh.loadIMG()

  bgScroll = 0

  koji = {
    x = 361,
    y = 536,
    image = gfx.koji,
    bullets = { }
  }

  bulletTimer = 0

  formations = {
    {
      timer = -1,
      objects = {
        shipProto(150, 150),
        shipProto(650, 150),
        shipProto(400, 450)
      }
    },
    {
      timer = 4,
      objects = {
        shipProto(32, 256),
        shipProto(256, 32),
        shipProto(128, 256)
      }
    }
  }

  fc = {
    mode = "in",
    current = 1,
    timer = 1
  }

  enemyTimer = 0

end


function bh.update(dt)

  -- Player movement

  bgScroll = (bgScroll + 500 * dt) % 1080

  local xMove = 0

  if lk.isDown('left') then xMove = -1 end
  if lk.isDown('right') then xMove = xMove + 1 end

  koji.x = koji.x + xMove * dt * 512

  -- Bullet control

  bulletTimer = math.max(0, bulletTimer - dt)

  if bulletTimer == 0 and lk.isDown(' ') then

    kojiShoot()

    bulletTimer = 0.2

  end

  updateBullets(dt)

  -- Update enemies in current formation
  for key, value in ipairs(formations[fc.current].objects) do
    value:update(dt)
  end

  -- Formation controlling

  if fc.mode == "in" then

    fc.timer = math.max(0, fc.timer - dt)

    if fc.timer == 0 then

      fc.mode = "fight"

      fc.timer = formations[fc.current].timer

    end

  elseif fc.mode == "fight" then

    if fc.timer > 0 then

      fc.timer = math.max(0, fc.timer - dt)

    end

    if fc.timer == 0 or #formations[fc.current].objects == 0 then

      fc.mode = "out"

      fc.timer = 1

    end

  elseif fc.mode == "out" then

    fc.timer = math.max(0, fc.timer - dt)

    if fc.timer == 0 then

      fc.current = fc.current + 1

      fc.mode = "in"

      fc.timer = 1

      if fc.current > #formations then

        love.event.quit()

      end

    end

  end

end


function bh.draw()

  lg.setColor(255, 255, 255)

  lg.draw(gfx.bgImage, 0, bgScroll)
  lg.draw(gfx.bgImage, 0, bgScroll - 1080)

  for key, frag in ipairs(koji.bullets) do
    frag:draw()
  end

  lg.draw(koji.image, koji.x, koji.y)

  -- Draw enemies in current formation
  for key, enemy in ipairs(formations[fc.current].objects) do
    enemy:draw()
  end

end



function kojiShoot()

  table.insert(koji.bullets,
    ammoPrimary(koji.x + koji.image:getWidth() / 2, koji.y + 3))

end



function updateBullets(dt)

  local bulletsToDelete = { }

  for bKey, frag in ipairs(koji.bullets) do

    frag:update(dt)

    -- Check formation

    local offset = getEnemyYOffset()

    for eKey, enemy in ipairs(formations[fc.current].objects) do

      if math.doesIntersect(enemy, frag) then

        if enemy:isVulnerable() then

          enemy.health = enemy.health - 1

          enemy:setVulnerable()

          if enemy.health == 0 then

            table.remove(formations[fc.current].objects, eKey)

          end

        end

        table.insert(bulletsToDelete, bKey)

      end

    end

    if frag.y < -36 then

      table.insert(bulletsToDelete, bKey)

    end

  end

  if #bulletsToDelete > 0 then

    for i = #bulletsToDelete, 0, -1 do
        table.remove(koji.bullets, i)
    end

  end

end


function getEnemyYOffset()

  local offset = 0

  if fc.mode == "in" then

    offset = -fc.timer * 600

  elseif fc.mode == "out" then

    offset = (1 - fc.timer) * 600

  end

  return offset

end
